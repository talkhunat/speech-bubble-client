# Speech Bubble Client

This project was made as a part of my thesis. Server application can be found here: https://gitlab.com/Talkhunat/speech-bubble-server 
If you have any sort of problems with running the app, feel free to contact me. 
You should also note that this is a work in progress and far from perfect, but it taught me many things and I'm looking forward to make it better!

## What you need
    1. Python (Tested with 3.6, but all versions above 3 should work.)
    2. Pip
    3. OpenSSL (I recommend using Windows subsystem for Linux and installing Ubuntu for example)
    4. Works on Windows/Ubuntu/OS X (Tested with Ubuntu and Windows)

## Setting up Speech Bubble Client
    1. Install requirements.txt with command 'pip install -r requirements.txt'
        --> Note: In case you get an error from 'pkg-resources==0.0.0', just delete it from requirements.txt and save.
        --> Note2: I recommend using virtualenvs when installing packages so that you don't get version conflicts with your systems global Python.
        --> Note3: If some package is missing from the requirements.txt, just install it with 'pip install packagenamehere'.
    2. Generate a certificate and private key with OpenSSL CLI
        --> You can use this command for example: 'openssl req -x509 -newkey rsa:4096 -nodes -keyout privatekey.key -out certificate.crt -days 365'
        --> Save certificate.crt to the folder 'certs/'
            --> Concatenate these files with command 'cat certificate.crt privatekey.key > cert.pem' to .pem format and place it to 
                (server application's folder)'app/certs/'-folder.
        --> *Make sure* that main-module in the root of the project has correct name for the certificate and that you gave correct host ip-address for the cert.

## Running the client
    1. Using command prompt, cd out of the project folder. Call 'python path-to-project-directory unique-id-in-numbers' 
        --> For example: 'python /home/user/speech-bubble-client/ 1234567890'
        --> When using the same id you may log in with different username since the server identifies clients by their id.
    2. If you see some sort of main window opening, you are all set! (Also make sure that server is also running)
