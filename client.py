import socket
import threading
import json
import sys
import traceback
import datetime
import ssl
from handlers.audiohandler import Audiohandler
from gui import gui
import queue
from collections import namedtuple

Response = namedtuple("Response", "name, timestamp, message, extra")

class Client:
    def __init__(self, uid, cafile):
        self.sock = None
        #FIXME: Ui should run in it's own thread.
        self.uid = uid
        self.window_update_queue = queue.Queue()
        self.purpose = ssl.Purpose.SERVER_AUTH
        self.context = ssl.create_default_context(self.purpose, cafile = cafile)
        self.audiohandler = Audiohandler()
        self.message_out_queue = queue.Queue()
        self.message_in_queue = queue.Queue()
        self.audio_out_queue = queue.Queue()
        self.signal_audiohandler_queue = queue.Queue()
        self.current_room=None
        self.ssl_sock=None
        self.name=None

    def connect_to_server(self, host, port, name, server_password):
        #FIXME: The print beneath should not print all the credentials.
        print(host + " " + port + " " + name + " " + server_password)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.settimeout(5)
            self.sock.connect((host, int(port)))
            self.sock.settimeout(None)
            self.ssl_sock = self.context.wrap_socket(self.sock, server_hostname=host)
            if self.authenticate(name, server_password):
                self.name = name
                self.window_update_queue.put({"auth_success":True})
                recv_messages_thread = threading.Thread(target=self.recv_messages, daemon=True)
                send_messages_thread = threading.Thread(target=self.send_messages, daemon=True)
                handle_recvd_messages_thread = threading.Thread(target=self.handle_recvd_messages, daemon=True)
                recv_messages_thread.start()
                send_messages_thread.start()
                handle_recvd_messages_thread.start()
                self.set_account_name(self.name)
                self.get_rooms()
            else:
                self.window_update_queue.put({"auth_success":False})
                self.ssl_sock.close()
        except TimeoutError:
            self.window_update_queue.put({"login_error":"Server connection timed out."})
        except OSError:
            self.window_update_queue.put({"login_error":"Unable to connect the server."})
        except ValueError:
            self.window_update_queue.put({"login_error":"Portnumber must be integer."})
        except Exception as e:
            tb = traceback.format_exc()
            print(str(tb))
            self.window_update_queue.put({"login_error":"Unhandled exception."})
            print(e)
            self.ssl_sock.close()
            print("Exiting app, cya!")
            sys.exit()

    def authenticate(self, name, server_password):
        data_dict = self.create_message_dict(name, "authenticate", server_password, self.uid)
        authentication_msg = self.prep_data(data_dict, True)
        self.ssl_sock.sendall(authentication_msg)
        data = bytearray()
        msg = ''
        while not msg:
            recvd = self.ssl_sock.recv(2048)
            if not recvd:
                print("Lost connection to the server")
                self.handle_exit()
            data = data + recvd
            if b'[\x00\x00\x00\x00]' in recvd:
                msg = data.replace(b'[\x00\x00\x00\x00]', b'')
        msg_stripped = msg.replace(b'[\x0B\x0B\x0B\x0B]', b'')
        aname, atimestamp, amessage, aextra = self.unwrap_data(msg_stripped.decode('utf-8'))
        if aextra[0] == 'True':
            return True
        else:
            return False

    def recv_messages(self):
        try:
            while True:
                data = bytearray()
                msg = ''
                while not msg:
                    recvd = self.ssl_sock.recv(1024)
                    if not recvd:
                        print("Lost connection to the server")
                        self.handle_exit()
                    data = data + recvd
                    if b'[\x00\x00\x00\x00]' in recvd:
                        msg = data.replace(b'[\x00\x00\x00\x00]', b'')
                self.message_in_queue.put(msg)
        except:
            tb =traceback.format_exc()
            print(str(tb))

    def set_account_name(self, name):
        name_msg_dict = self.create_message_dict(name, "setname")
        name_msg = self.prep_data(name_msg_dict, True)
        self.message_out_queue.put(name_msg)

    def get_rooms(self):
        data_dict = self.create_message_dict(self.name, "getrooms")
        msg = self.prep_data(data_dict, True)
        self.message_out_queue.put(msg)

#TODO: Reconnect if unexpected disconnect happens
    def handle_recvd_messages(self):
        while True:
            try:
                msg = self.message_in_queue.get()
                if b'[\x1A\x1A\x1A\x1A]' in msg:
                    msg_stripped = msg.replace(b'[\x1A\x1A\x1A\x1A]', b'')
                    self.audio_out_queue.put(msg_stripped)
                elif b'[\x0B\x0B\x0B\x0B]' in msg:
                    msg_stripped = msg.replace(b'[\x0B\x0B\x0B\x0B]', b'')
                    name, timestamp, message, extra = self.unwrap_data(msg_stripped.decode('utf-8'))
                    self.window_update_queue.put(Response(name, timestamp, message, extra))
                elif b'[\x1B\x1B\x1B\x1B]' in msg:
                    msg_stripped = msg.replace(b'[\x1B\x1B\x1B\x1B]', b'')
                    name, timestamp, message, extra = self.unwrap_data(msg_stripped.decode('utf-8'))
                    self.window_update_queue.put(Response(name, timestamp, message, extra))
                else:
                    name, timestamp, message, extra = self.unwrap_data(msg.decode('utf-8'))
                    self.window_update_queue.put(Response(name, timestamp, message, extra))
            except Exception:
                tb = traceback.format_exc()
                print(str(tb))
                print("Message received when error occurred: ",msg)

    def send_text_messages(self, message):
        data_dict = self.create_message_dict(self.name, message)
        msg = self.prep_data(data_dict)
        self.message_out_queue.put(msg)

    def join_voice_chat(self):
        data_dict = self.create_message_dict(self.name, "joinvoip")
        msg = self.prep_data(data_dict, True)
        self.message_out_queue.put(msg)

    def leave_voice_chat(self):
        data_dict = self.create_message_dict(self.name, "leavevoip")
        msg = self.prep_data(data_dict, True)
        self.message_out_queue.put(msg)

    def start_call(self):
        print("Call started..")
        handle_audio_thread = threading.Thread(
                    target=self.audiohandler.start_stream, 
                    args=(self.audio_out_queue, self.message_out_queue, self.signal_audiohandler_queue)
                    )
        handle_audio_thread.start()

    def end_call(self):
        self.signal_audiohandler_queue.put("END")
        print("Ending call..")

    def create_room(self, roomname):
        roomname = self.format_string(roomname)
        data_dict = self.create_message_dict(self.name, "createroom", roomname)
        msg = self.prep_data(data_dict, True)
        self.message_out_queue.put(msg)

    def switch_room(self, destination):
        destination = self.format_string(destination)
        data_dict = self.create_message_dict(self.name, "switchroom", destination)
        msg = self.prep_data(data_dict, True)
        self.message_out_queue.put(msg)

    def send_messages(self):
        while True:
            try:
                message = self.message_out_queue.get()
                self.ssl_sock.sendall(message)
            except Exception as e:
                print(e)

    def create_message_dict(self, name, message, *extra):
        timestamp = self.get_datetime()
        data_dict = {
            "name": name,
            "timestamp": timestamp,
            "message": message,
            "extra": extra
        }
        return data_dict

    def get_datetime(self):
        dt = datetime.datetime.now()
        dt_string = dt.strftime("%d.%m.%y, %H:%M")
        return dt_string

    def prep_data(self, data_dict, *args):
        if len(args) == 0:
            data_str = json.dumps(data_dict,ensure_ascii=False)
            data_str = data_str + '[\x00\x00\x00\x00]'
            return data_str.encode('utf-8')
        elif args[0] == True:
            data_str = json.dumps(data_dict, ensure_ascii=False)
            data_str = data_str + '[\x0B\x0B\x0B\x0B]' + '[\x00\x00\x00\x00]'
            return data_str.encode('utf-8')

    def unwrap_data(self, message):
        data_dictionary = json.loads(message)
        senders_name = data_dictionary.get("name")
        timestamp = data_dictionary.get("timestamp")
        senders_message = data_dictionary.get("message")
        senders_extra = data_dictionary.get("extra")
        return (senders_name, timestamp, senders_message, senders_extra)

    def format_string(self, message_str):
        lowercase_str = message_str.lower()
        formatted = ''.join([i for i in lowercase_str if i.isalpha() or i.isdigit()])
        return formatted

    def handle_exit(self):
        self.ssl_sock.close()
        print("Exiting app, cya!")
        sys.exit()