from gui import gui

class EventHandler:
    def __init__(self, root, client):
        self.root = root
        self.client = client
        self.calling = False

    def exit_app(self):
        print("in exit app.")
        #FIXME: Quitting mainwindow leaves the ui waiting for other threads to be shut down.
        #Using root.destroy() destroys the window, but threads are still there.
        self.root.quit()

    def send_message(self, message):
        sender = self.client.name
        self.toggle_textbox_state(False)
        self.root.home_window.chat_frame.text_box.insert(gui.tk.END, sender + ": " + message + "\n")
        self.toggle_textbox_state(True)
        self.client.send_text_messages(message)

    def toggle_call(self):
        if self.calling == False:
            self.client.join_voice_chat()
            self.toggle_callbutton_state(True)
        else:
            self.client.leave_voice_chat()
            self.toggle_callbutton_state(False)

    def logout(self):
        pass

    def hide_login_window(self):
        self.root.login_window.pack_forget()

    def restore_login_window(self):
        self.root.loading_window.pack_forget()
        self.root.login_window.pack()

    def show_loading_window(self, text):
        self.root.loading_window.update_loading_text(text)
        self.root.loading_window.pack()

    def show_home_window(self):
        self.root.config(menu=self.root.home_window.menubar)
        self.root.loading_window.pack_forget()
        self.root.home_window.pack(fill=gui.tk.BOTH, expand=1)

    def show_message_window(self, text, islogin):
        self.message_window = gui.MessageWindow(text)
        if islogin:
            self.message_window.protocol("WM_DELETE_WINDOW", self.close_message_window_on_login)
        else:
            self.message_window.protocol("WM_DELETE_WINDOW", self.message_window.destroy)
    

    def close_message_window_on_login(self):
        self.message_window.destroy()
        self.restore_login_window()

    def show_create_room_window(self):
        self.create_room_window = gui.CreateRoomWindow(self.root, self.client.create_room, self.client.get_rooms)


    def init_chatroom_listbox_items(self, rooms):
        listbox_length= self.root.home_window.chatroom_listbox_frame.listbox.size()
        for room in rooms:
            if room in self.root.home_window.chatroom_listbox_frame.listbox.get(0, listbox_length):
                continue
            else:
                self.root.home_window.chatroom_listbox_frame.listbox.insert(gui.tk.END, room)

    def change_current_listbox_room(self, previous, new):
        listbox_length = self.root.home_window.chatroom_listbox_frame.listbox.size()
        index = 0
        while index < listbox_length:
            if self.root.home_window.chatroom_listbox_frame.listbox.get(index) == new:
                self.root.home_window.chatroom_listbox_frame.listbox.itemconfig(index, {"fg":"blue"})
                self.client.current_room = new
                self.root.home_window.chat_frame.room_header_var.set(new)
            if self.root.home_window.chatroom_listbox_frame.listbox.get(index) == previous:
                self.root.home_window.chatroom_listbox_frame.listbox.itemconfig(index, {"fg":"black"})
            index +=1

    def switch_room(self, evt):
        if self.calling:
            self.toggle_call()
        widget = evt.widget
        index = int(widget.curselection()[0])
        previous = self.client.current_room
        print("previous: ", previous)
        new = widget.get(index)
        self.client.switch_room(new)
        print("new:", new)
        if previous != new:
            self.toggle_textbox_state(False)
            self.root.home_window.chat_frame.text_box.delete(1.0, gui.tk.END)
            self.toggle_textbox_state(True)

    def handle_update(self, item):
        if "login_error" in item:
            self.show_message_window(item["login_error"], True)
        elif "auth_success" in item:
            if item["auth_success"]:
                self.show_home_window()
            else:
                self.show_message_window("Incorrect password", True)
        elif "getrooms_response" in item:
            if self.client.current_room == None:
                self.client.current_room="mainlobby"
            self.root.home_window.chat_frame.room_header_var.set(self.client.current_room)
            self.root.home_window.chat_frame.participants_var.set(item.extra[2])
            self.init_chatroom_listbox_items(item.extra[1])
            self.change_current_listbox_room(None, item.extra[0])
        elif "roomswitch_response" in item:
            if item.extra[0] == True:
                previous = self.client.current_room
                self.client.current_room = item.extra[1]
                self.root.home_window.chat_frame.room_header_var.set(self.client.current_room)
                self.root.home_window.chat_frame.participants_var.set(item.extra[2])
                self.change_current_listbox_room(previous, item.extra[1])
            else:
                self.show_message_window("Room not found.", False)
        elif "joinvoip_response" in item:
            if item.extra[0] == True:
                self.toggle_callbutton_state(False)
                self.client.start_call()
                self.root.home_window.chat_frame.call_button_var.set("End call")
                self.root.home_window.chat_frame.call_button.config(bg="#ff6565")
                self.calling = True
        elif "leavevoip_response" in item:
            if item.extra[0] == True:
                self.client.end_call()
                self.toggle_callbutton_state(False)
                self.root.home_window.chat_frame.call_button_var.set("Start call")
                self.root.home_window.chat_frame.call_button.config(bg="#05c890")
                self.calling = False
        else:
            self.toggle_textbox_state(False)
            self.root.home_window.chat_frame.text_box.insert(gui.tk.END, item.name + ": " + item.message + "\n")
            self.toggle_textbox_state(True)

    def toggle_textbox_state(self, disabled):
        if disabled:
            self.root.home_window.chat_frame.text_box.config(state="disabled")
        else:
            self.root.home_window.chat_frame.text_box.config(state="normal")

    def toggle_callbutton_state(self, disabled):
        if disabled:
            self.root.home_window.chat_frame.call_button.config(state="disabled")
        else:
            self.root.home_window.chat_frame.call_button.config(state="normal")

    def sendbutton_callback(self, event):
        self.send_message(self.root.home_window.chat_frame.textbox_entry.get())
        self.root.home_window.chat_frame.textbox_entry.delete(0, "end")



