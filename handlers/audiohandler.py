import pyaudio
import threading
import queue
import time

class Audiohandler:
    def __init__(self):
        self.WIDTH = 2
        self.CHANNELS = 2
        self.RATE = 16000
        self.stream = None
        self.audio_out_queue = None
        self.message_out_queue = None
        self.signal_audiohandler_queue = None

    def audio_callback(self, in_data, frame_count, time_info, status):
        self.pass_audiodata(in_data)
        try:
            data_to_play = self.audio_out_queue.get(block=False)
            bytedata = bytes(data_to_play)
        except queue.Empty:
            bytedata = b'\x00' * (frame_count * 2 * 2)
        return(bytedata, pyaudio.paContinue)

    def start_stream(self, audio_out_queue, message_out_queue, signal_audiohandler_queue):
        self.p = pyaudio.PyAudio()
        self.audio_out_queue = audio_out_queue
        self.message_out_queue = message_out_queue
        self.signal_audiohandler_queue = signal_audiohandler_queue
        self.stream = self.p.open(
            format=self.p.get_format_from_width(self.WIDTH),
            channels = self.CHANNELS,
            rate = self.RATE,
            input=True,
            output=True,
            stream_callback = self.audio_callback
        )
        self.stream.start_stream()
        item = self.signal_audiohandler_queue.get()
        self.stream.stop_stream()
        print("stream stopped")
        self.stream.close()
        self.p.terminate()
        print("Call ended.")

    def pass_audiodata(self, data):
        data = data + b'[\x1A\x1A\x1A\x1A]' + b'[\x00\x00\x00\x00]'
        self.message_out_queue.put(data)