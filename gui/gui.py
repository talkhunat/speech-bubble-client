import tkinter as tk
import queue
import threading
from handlers import eventhandler as eh

class MainWindow(tk.Tk):
    def __init__(self, client, window_update_queue, settings, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.settings = settings
        self.handler = eh.EventHandler(self, client)
        self.update_queue = window_update_queue
        self.client = client
        self.login_window = LoginWindow(self, client, self.handler, self.settings)
        self.loading_window = LoadingWindow(self)
        self.home_window = HomeWindow(self, client, self.handler, self.settings,
            width=self.settings["gui"]["HomeWindow"]["width"], 
            height=self.settings["gui"]["HomeWindow"]["height"])
        #self.home_window.pack_propagate(0)
        self.config(menu=self.login_window.menubar)

    def update_window(self):
        try:
            item = self.update_queue.get_nowait()
            if item is not None:
                self.handler.handle_update(item)
        except queue.Empty:
            pass
        self.after(100, self.update_window)

class LoginWindow(tk.Frame):
    def __init__(self, root, client, handler, settings, *args, **kwargs):
        tk.Frame.__init__(self, root, *args, **kwargs)
        #TODO: Create stringvariables for each entry and label.
        self.settings = settings
        self.bg=self.settings["gui"]["LoginWindow"]["bg"]
        self.config(bg=self.bg)
        self.font=(self.settings["gui"]["fonts"]["label_font"]["font_family"],
            self.settings["gui"]["fonts"]["label_font"]["font_size"])
        self.fg=self.settings["gui"]["fonts"]["label_font"]["font_foreground_color"]
        self.pack()
        self.handler = handler
        self.client = client
        self.menubar = tk.Menu(self, tearoff=0)
        self.filemenu = LoginFileMenu(self.menubar, root, self.handler)
        self.menubar.add_cascade(label="File", menu=self.filemenu)

        self.ip_entry_label = tk.Label(self, text="IP-address:")
        self.ip_entry_label.config(
            font=self.font, 
            fg=self.fg,
            bg=self.bg)
        self.ip_entry_label.pack(padx=self.settings["gui"]["LoginWindow"]["ip_padx"])
        self.ip_entry = tk.Entry(self)
        self.ip_entry.pack(padx=self.settings["gui"]["LoginWindow"]["ip_padx"])

        self.port_entry_label = tk.Label(self, text="Port:")
        self.port_entry_label.config(
            font=self.font, 
            fg=self.fg,
            bg=self.bg)
        self.port_entry_label.pack(padx=self.settings["gui"]["LoginWindow"]["port_padx"])
        self.port_entry = tk.Entry(self)
        self.port_entry.pack(padx=self.settings["gui"]["LoginWindow"]["port_padx"])

        self.name_entry_label = tk.Label(self, text="Nickname:")
        self.name_entry_label.config(
            font=self.font,
            fg=self.fg,
            bg=self.bg)
        self.name_entry_label.pack(padx=self.settings["gui"]["LoginWindow"]["name_padx"])
        self.name_entry = tk.Entry(self)
        self.name_entry.pack(padx=self.settings["gui"]["LoginWindow"]["name_padx"])

        self.password_entry_label = tk.Label(self, text="Server password:")
        self.password_entry_label.config(
            font=self.font, 
            fg=self.fg,
            bg=self.bg)
        self.password_entry_label.pack(
            padx=self.settings["gui"]["LoginWindow"]["password_padx"])
        self.password_entry = tk.Entry(self, show="*")
        self.checkvar = tk.IntVar()
        self.password_entry.pack(padx=self.settings["gui"]["LoginWindow"]["password_padx"])
        self.bookmark_c = tk.Checkbutton(self, text="Save to bookmarks", variable=self.checkvar, onvalue=1, offvalue=0)
        self.bookmark_c.config(fg=self.fg, bg=self.bg)
        self.bookmark_c.pack()

        self.submit_button = tk.Button(self, text="Login", 
            command=lambda: [root.handler.hide_login_window(),
            root.handler.show_loading_window("Logging in.."),
            threading.Thread(
                target=lambda: self.client.connect_to_server(self.ip_entry.get(), 
                self.port_entry.get(), 
                self.name_entry.get(), 
                self.password_entry.get())).start()])
        self.submit_button.pack()

class HomeWindow(tk.Frame):
    def __init__(self, root, client, handler, settings, *args, **kwargs):
        tk.Frame.__init__(self, root,*args, **kwargs)
        self.settings = settings
        self.handler = handler
        self.client = client
        self.menubar = tk.Menu(self, tearoff=0)
        self.filemenu = FileMenu(self.menubar, root, self.handler)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.chatroom_listbox_frame = ChatRoomListboxFrame(self, self.client, self.handler)
        self.chatroom_listbox_frame.pack(side=tk.LEFT, fill=tk.Y)
        self.chat_frame=ChatFrame(self, self.client, self.handler)
        self.chat_frame.pack(side=tk.TOP)

class ChatFrame(tk.Frame):
    def __init__(self, root, client, handler):
        tk.Frame.__init__(self, root)
        self.handler = handler
        self.room_header_var = tk.StringVar()
        self.room_header_label = tk.Label(self, textvariable=self.room_header_var)
        self.room_header_var.set(client.current_room)
        self.room_header_label.pack(side=tk.TOP)

        self.participants_var = tk.StringVar()
        self.participants_label = tk.Label(self, textvariable=self.participants_var)
        self.participants_var.set("asd")
        self.participants_label.pack(side=tk.TOP)

        self.text_box = tk.Text(self, height=20, width=60)
        self.text_box.pack(side=tk.TOP)
        self.text_box.config(state="disabled")

        self.textbox_entry = tk.Entry(self)
        self.textbox_entry.bind('<Return>', self.handler.sendbutton_callback)
        self.textbox_entry.pack(side=tk.LEFT)
        self.sendbutton = tk.Button(self, text="Send", command=lambda: self.handler.sendbutton_callback(None))
        self.sendbutton.pack(side=tk.LEFT)

        self.call_button_var = tk.StringVar()
        self.call_button = tk.Button(self, textvariable=self.call_button_var, command=self.handler.toggle_call)
        self.call_button_var.set("Start Call")
        self.call_button.config(bg="#05c890")
        self.call_button.pack(side=tk.LEFT)

class ChatRoomListboxFrame(tk.Frame):
    def __init__(self, root, client, handler):
        tk.Frame.__init__(self, root)
        self.handler = handler
        self.listbox = tk.Listbox(self)
        self.listbox.bind('<Double-Button-1>', self.handler.switch_room)
        self.listbox.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        self.create_room_button = tk.Button(self, 
            text="Create new room", 
            command=self.handler.show_create_room_window)
        self.create_room_button.pack(side=tk.TOP)

class LoadingWindow(tk.Frame):
    def __init__(self, root, *args, **kwargs):
        tk.Frame.__init__(self, root, *args, **kwargs)
        self.loading_text_var = tk.StringVar()
        self.loading_text_label = tk.Label(self, textvariable=self.loading_text_var)
        self.loading_text_label.pack()

    def update_loading_text(self, loadingtext):
        self.loading_text_var.set(loadingtext)

class LoginFileMenu(tk.Menu):
    def __init__(self, parent, root, handler):
        tk.Menu.__init__(self, master=parent, tearoff=0)
        self.handler = handler
        self.add_command(label="Bookmarks", command=self.handler.hide_login_window)
        self.add_command(label="Login", command=self.handler.restore_login_window)
        self.add_command(label="Exit", command=self.handler.exit_app)

class FileMenu(tk.Menu):
    def __init__(self, parent, root, handler):
        tk.Menu.__init__(self, master=parent, tearoff=0)
        self.handler = handler
        self.add_command(label="Logout", command=self.handler.logout)
        self.add_command(label="Exit", command=self.handler.exit_app)

class MessageWindow(tk.Toplevel):
    def __init__(self, message):
        tk.Toplevel.__init__(self)
        self.minsize(200, 200)
        self.frame = tk.Frame(self)
        self.frame.pack()
        self.text_var = tk.StringVar()
        self.message = tk.Message(self.frame, textvariable=self.text_var)
        self.message.pack(side=tk.TOP)
        self.text_var.set(message)

class CreateRoomWindow(tk.Toplevel):
    def __init__(self, root, create_room, get_rooms):
        tk.Toplevel.__init__(self, root)
        self.handler = root.handler
        self.minsize(200, 200)
        self.frame = tk.Frame(self)
        self.frame.pack()
        self.text_var = tk.StringVar()
        self.roomname_entry_label=tk.Label(self.frame, text="Give a name for the new room:")
        self.roomname_entry_label.pack(side=tk.TOP)
        self.roomname = tk.Entry(self.frame, textvariable=self.text_var)
        self.roomname.pack(side=tk.TOP)
        self.submit_button = tk.Button(self.frame, 
            text="Create", 
            command=lambda: [create_room(self.roomname.get()), get_rooms(), self.handler.toggle_call() ,self.destroy()])
        self.submit_button.pack(side=tk.BOTTOM)