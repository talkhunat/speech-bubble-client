import client
import sys
import os
import json
from gui import gui

uid = sys.argv[1]
basedir = os.path.abspath(os.path.dirname(__file__))
cafile = os.path.join(basedir, 'certs/localwifi.crt')
settingsjson = os.path.join(basedir, 'settings.json')
with open(settingsjson) as json_data:
    settings = json.load(json_data)
cl = client.Client(uid, cafile)
app = gui.MainWindow(cl, cl.window_update_queue, settings)
app.after(100, app.update_window)
app.mainloop()
